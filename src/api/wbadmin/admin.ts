import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

//定义接口地址
enum Api {
  Login = '/admin/public/login',
  Logout = '/admin/index/logout',
  GetUserInfo = '/admin/index/getAdminInfo',
  GetPermCode = '/admin/index/getPermCode',

  //获取管理员左侧菜单接口
  GetMenuList = '/admin/index/getAdminMenuList',

  //菜单管理接口
  GetAllMenuList = '/admin/Rules/getList',
  MenuAdd = '/admin/Rules/addMenu',
  MenuEdit = '/admin/Rules/editMenu',
  MenuDel = '/admin/Rules/delMenu',

  //角色管理
  RoleListByPage = '/admin/Rules/getRoleList',
  RoleAllList = '/admin/Rules/getRoleAllList',
  RoleEdit = '/admin/Rules/editRole',
  RoleAdd = '/admin/Rules/addRole',
  RoleDel = '/admin/Rules/delRole',

  //管理员管理
  GetAdminList = '/admin/Rules/getAdminList',
  AdminEdit = '/admin/Rules/editAdmin',
  AdminAdd = '/admin/Rules/addAdmin',
  AdminDel = '/admin/Rules/delAdmin',
  UpdatePassword = '/admin/index/updPassword',

  //获取首页统计数据
  GetIndexData = '/admin/index/getIndexData',
}

//用户登陆
export function loginApi(params, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
//获取用户信息
export function getUserInfo() {
  return defHttp.get({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}
//获取权限code
export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}
//退出
export function doLogout() {
  return defHttp.get({ url: Api.Logout });
}
//获取管理员菜单
export const getMenuList = () => {
  return defHttp.get({ url: Api.GetMenuList });
};

//菜单管理-菜单列表
export const getAllMenuList = () => {
  return defHttp.get({ url: Api.GetAllMenuList });
};

export const menuAdd = (params) => defHttp.post({ url: Api.MenuAdd, params });
export const menuEdit = (params) => defHttp.post({ url: Api.MenuEdit, params });
export const menuDel = (params) => defHttp.post({ url: Api.MenuDel, params });

export const getRoleListByPage = (params?) => defHttp.post({ url: Api.RoleListByPage, params });
export const getRoleAllList = (params?) => defHttp.post({ url: Api.RoleAllList, params });
export const roleEdit = (params) => defHttp.post({ url: Api.RoleEdit, params });
export const roleAdd = (params) => defHttp.post({ url: Api.RoleAdd, params });
export const roleDel = (params) => defHttp.post({ url: Api.RoleDel, params });

export const getAdminList = (params?) => {
  return defHttp.get({ url: Api.GetAdminList, params });
};
export const adminEdit = (params) => defHttp.post({ url: Api.AdminEdit, params });
export const adminAdd = (params) => defHttp.post({ url: Api.AdminAdd, params });
export const adminDel = (params) => defHttp.post({ url: Api.AdminDel, params });

export const updatePassword = (params) => defHttp.post({ url: Api.UpdatePassword, params });

export const getIndexData = (params?) => {
  return defHttp.get({ url: Api.GetIndexData, params });
};
