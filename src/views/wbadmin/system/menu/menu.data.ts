import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Icon } from '/@/components/Icon';

export const columns: BasicColumn[] = [
  {
    title: '菜单名称',
    dataIndex: 'title',
    width: 200,
    align: 'left',
  },
  {
    title: '图标',
    dataIndex: 'icon',
    width: 50,
    customRender: ({ record }) => {
      return h(Icon, { icon: record.icon });
    },
  },
  {
    title: '组件',
    dataIndex: 'component',
  },
  {
    title: 'path',
    dataIndex: 'path',
  },
  {
    title: '类型',
    dataIndex: 'type',
    customRender: ({ record }) => {
      if (record.type == 1) {
        return '菜单';
      } else {
        return '权限';
      }
    },
  },
  {
    title: '排序',
    dataIndex: 'sort',
    width: 50,
  },
  {
    title: '是否显示',
    dataIndex: 'hide_menu',
    width: 80,
    customRender: ({ record }) => {
      const status = record.hide_menu;
      const enable = ~~status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '显示' : '隐藏';
      return h(Tag, { color: color }, () => text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'title',
    label: '菜单名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  // {
  //   field: 'status',
  //   label: '状态',
  //   component: 'Select',
  //   componentProps: {
  //     options: [
  //       { label: '启用', value: '0' },
  //       { label: '停用', value: '1' },
  //     ],
  //   },
  //   colProps: { span: 8 },
  // },
];

export const formSchema: FormSchema[] = [
  {
    field: 'type',
    label: '菜单类型',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '菜单', value: 1 },
        { label: '权限', value: 2 },
      ],
    },
    colProps: { lg: 24, md: 24 },
  },
  {
    field: 'title',
    label: '菜单名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'id',
    label: 'ID',
    show: false,
    component: 'InputNumber',
  },
  {
    field: 'pid',
    label: '上级菜单',
    component: 'TreeSelect',
    componentProps: {
      fieldNames: {
        label: 'title',
        key: 'id',
        value: 'id',
      },
      getPopupContainer: () => document.body,
    },
  },
  {
    field: 'sort',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 0,
    required: false,
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    required: false,
  },

  {
    field: 'path',
    label: '路由地址(path)',
    component: 'Input',
    required: false,
  },
  {
    field: 'name',
    label: 'Name(全局唯一)',
    component: 'Input',
    required: false,
  },
  {
    field: 'component',
    label: '组件路径(component)',
    component: 'Input',
    colProps: { lg: 24, md: 24 },
  },
  {
    field: 'hide_menu',
    label: '显示状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '显示', value: 0 },
        { label: '隐藏', value: 1 },
      ],
    },
  },
];
